package com.rappasocial.eyesbreak;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.text.DateFormat;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ParseException;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class ServiceEyesBreak extends Service{
	
	boolean bMondey, bTUESDAY, bWEDNESDAY, bTHURSDAY, bFRIDAY, bSATURDAY, bSUNDAY;
	
@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
//	    Toast.makeText(this, "false ", Toast.LENGTH_SHORT).show();
	    alarmScheduleManager.cancel(alarmScheduleIntentBreak);
	    alarmScheduleManager.cancel(alarmScheduleIntentToWork);
		super.onDestroy();
	}

	//	public Notification scheduleNotification;
    public AlarmManager alarmScheduleManager;
    public PendingIntent alarmScheduleIntentBreak;
    public PendingIntent alarmScheduleIntentToWork;
    private Boolean autoUpdateBoolean = true;
    private int intervalsGoneByInt = 0;
//    private Notification notification;
    public static final int NOTIFICATION_ID = 1;
    public static final String PREFS_NAME = "com.rappasocial.eyesbreak.EyesBreakPrefs";
    private final IBinder mBinder = new MyBinder();
    
	@Override
	public void onCreate(){
		
		super.onCreate(); 
		
//		Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
		
//		  TODO: Actions to perform when service is created.
//        int icon = R.drawable.icon;
        String tickerText = "INTERVAL FIRED";
        long when = System.currentTimeMillis();
//        scheduleNotification = new Notification(icon, tickerText, when);

        alarmScheduleManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        String ALARM_ACTION;
        ALARM_ACTION = ScheduleAlarmReceiver.ACTION_REFRESH_SCHEDULE_ALARM;
        Intent intentBreakAlarm = new Intent(ALARM_ACTION);
        intentBreakAlarm.putExtra("TostText", getString(R.string.TakeAnEyesBreak));
        Intent intentToWorkAlarm = new Intent(ALARM_ACTION);
        intentToWorkAlarm.putExtra("TostText", getString(R.string.BackToWork));
        alarmScheduleIntentBreak = PendingIntent.getBroadcast(this, 0, intentBreakAlarm,
        0);
        alarmScheduleIntentToWork = PendingIntent.getBroadcast(this, 1, intentToWorkAlarm,
                0);
        
         
        
//        android.os.Debug.waitForDebugger();
		
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
	    Bundle extras = arg0.getExtras();
	    Log.d("service","onBind");

	    return mBinder;
	}

	public class MyBinder extends Binder {
		ServiceEyesBreak getService() {
	        return ServiceEyesBreak.this;
	    }
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{	
		
		SharedPreferences mySharedPreferences = getSharedPreferences(PREFS_NAME, 0);
		
		
		
		long WorkIntervalms = mySharedPreferences.getLong(
				"WorkTime", 10000);
		
		long RestIntervalms = mySharedPreferences.getLong(
				"RestTime", 10000);
		
		long InitTime = mySharedPreferences.getLong(
				"InitTime", 10000);
		
		long ShutTime = mySharedPreferences.getLong(
				"ShutTime", 10000);
		
		

		

//		Date intervalTimeAsDateObject = null;
//		long updateFreqMilliLong;
//		try {
//			intervalTimeAsDateObject = dfInterval.parse(updateFreq);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		updateFreqMilliLong = intervalTimeAsDateObject.getTime() - 18000000;

	
			
//			Toast.makeText(this, "true ", Toast.LENGTH_SHORT).show();
			
			int alarmType = AlarmManager.RTC_WAKEUP;
//			long timetoRefresh = SystemClock.elapsedRealtime()
//					+ updateFreqMilliLong;
//			System.currentTimeMillis()
			
//			if (Calendar.WEDNESDAY == calendar.get(Calendar.DAY_OF_WEEK))
//				calendar.getTimeInMillis()
		
			
			
	  
			long curTime_ms = System.currentTimeMillis() + getShiftDaysToStartInMillis();


			Calendar calendar = Calendar.getInstance();
			Calendar calendar1 = Calendar.getInstance();
			Calendar calendar2 = Calendar.getInstance();
			// if (Calendar.WEDNESDAY == calendar.get(Calendar.DAY_OF_WEEK))
			// calendar.getTimeInMillis()
			
			calendar.setTimeInMillis(curTime_ms);
			calendar1.setTimeInMillis(InitTime);
			calendar2.setTimeInMillis(ShutTime);
			long curTime_msTimeOnly = calendar.get(Calendar.HOUR_OF_DAY)*1000*60*60 + calendar.get(Calendar.MINUTE)*1000*60;
			long InitTimeOnlyTime = calendar1.get(Calendar.HOUR_OF_DAY)*1000*60*60 + calendar1.get(Calendar.MINUTE)*1000*60;
            long ShutTimeOnlyTime = calendar2.get(Calendar.HOUR_OF_DAY)*1000*60*60 + calendar2.get(Calendar.MINUTE)*1000*60;
//			calendar.set(Calendar.YEAR, 2013); //alarmHour from TextView
//			calendar.set(Calendar.MONTH, 4); //alarmMinute from TextView
//			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0); 
			calendar.set(Calendar.MINUTE, 0); 
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			InitTime = InitTime + calendar.getTimeInMillis();
			ShutTime = ShutTime + calendar.getTimeInMillis();
			//curTime_msTimeOnly = curTime_ms - calendar.getTimeInMillis();
			long timetoRefresh = WorkIntervalms + RestIntervalms;
			long timetoStartalarmBreak = InitTime + WorkIntervalms;
			long timetoStartalarmToWork = InitTime + WorkIntervalms + RestIntervalms;
			
			 
			long timecurshift;		
			if (getShiftDaysToStartInMillis() == 0 
			&& curTime_ms >= InitTime
			&& curTime_ms <= ShutTime) {
				timecurshift = (WorkIntervalms + RestIntervalms) * (long) Math.floor((double) (curTime_ms - InitTime) / (WorkIntervalms + RestIntervalms) );
				timetoStartalarmBreak = timetoStartalarmBreak + timecurshift;
				timetoStartalarmToWork = timetoStartalarmToWork + timecurshift;
				
			}		
			
//			getDate(mySharedPreferences.getLong("InitTime", 10000) + calendar.getTimeInMillis(), "dd/MM/yyyy hh:mm:ss.SSS")
//			getDate(timetoStartalarmToWork, "dd/MM/yyyy hh:mm:ss.SSS")
			String datata = getDate(timetoStartalarmBreak, "dd/MM/yyyy hh:mm:ss.SSS");
			alarmScheduleManager.setRepeating(AlarmManager.RTC_WAKEUP, timetoStartalarmBreak,
					WorkIntervalms + RestIntervalms, alarmScheduleIntentBreak);
			
			alarmScheduleManager.setRepeating(AlarmManager.RTC_WAKEUP, timetoStartalarmToWork,
					WorkIntervalms + RestIntervalms, alarmScheduleIntentToWork);


		return Service.START_NOT_STICKY;
		
	}
	
	public long getShiftDaysToStartInMillis() {

//		Calendar calendar = Calendar.getInstance();
//		calendar.setTimeInMillis(System.currentTimeMillis());
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		bMondey = settings.getBoolean("chbMondey", true);
		bTUESDAY = settings.getBoolean("chbTuesday", true);
		bWEDNESDAY = settings.getBoolean("chbWednesday", true);
		bTHURSDAY = settings.getBoolean("chbThursday", true);
		bFRIDAY = settings.getBoolean("chbFriday", true);
		bSATURDAY = settings.getBoolean("chbSaturday", true);
		bSUNDAY = settings.getBoolean("chbSunday", true);
		
		

		long WorkIntervalms = settings.getLong("WorkTime", 10000);

		long RestIntervalms = settings.getLong("RestTime", 10000);

		long InitTime = settings.getLong("InitTime", 10000);

		long ShutTime = settings.getLong("ShutTime", 10000);

		

		long curTime_ms = System.currentTimeMillis();

		Calendar calendar = Calendar.getInstance();

		// if (Calendar.WEDNESDAY == calendar.get(Calendar.DAY_OF_WEEK))
		// calendar.getTimeInMillis()

		calendar.setTimeInMillis(curTime_ms);
		
		

		// calendar.set(Calendar.YEAR, 2013); //alarmHour from TextView
		// calendar.set(Calendar.MONTH, 4); //alarmMinute from TextView
		// calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		InitTime = InitTime + calendar.getTimeInMillis();
		ShutTime = ShutTime + calendar.getTimeInMillis();
		
		long Time_msNext;
		long timecurshift;
		if (curTime_ms >= InitTime && curTime_ms <= ShutTime) {
			timecurshift = (WorkIntervalms + RestIntervalms)
					* (long) Math.floor((double) (curTime_ms - InitTime)
							/ (WorkIntervalms + RestIntervalms));
//			InitTime = InitTime + timecurshift;

			if (curTime_ms - (InitTime + timecurshift) > WorkIntervalms) {

				Time_msNext = InitTime + timecurshift + WorkIntervalms
								+ RestIntervalms;

			} else {

				Time_msNext = InitTime + timecurshift + WorkIntervalms;

			}

		} else {

			Time_msNext = curTime_ms;

		}
		
		

		int numberOfCurrDay = calendar.get(Calendar.DAY_OF_WEEK);
		boolean daysarrayBool[] = { false, bSUNDAY, bMondey, bTUESDAY,
				bWEDNESDAY, bTHURSDAY, bFRIDAY, bSATURDAY };
		int j = numberOfCurrDay;
		if (Time_msNext > ShutTime) { j = numberOfCurrDay + 1 ; if (numberOfCurrDay == 7) j = 1; }
		
		for (int i = j; i <= 7; i++) {

			if (daysarrayBool[i]) {

				return (i - j + (Time_msNext > ShutTime ? 1:0)) * 1000 * 60 * 60 * 24;

			}
		}

		for (int i = 1; i <= numberOfCurrDay; i++) {

			if (daysarrayBool[i]) {

				return (7 - j + i + (Time_msNext > ShutTime ? 1:0)) * 1000 * 60 * 60 * 24;

			}

		}

		return 0;

	}
	
	public static String getDate(long milliSeconds, String dateFormat)
	{
	    // Create a DateFormatter object for displaying date in specified format.
	    DateFormat formatter = new SimpleDateFormat(dateFormat);

	    // Create a calendar object that will convert the date and time value in milliseconds to date. 
	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}
	
	

}
