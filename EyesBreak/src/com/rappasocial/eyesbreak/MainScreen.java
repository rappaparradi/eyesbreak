package com.rappasocial.eyesbreak;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.holoeverywhere.app.AlertDialog;

import com.actionbarsherlock.app.SherlockActivity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings.Secure;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.TimePicker.OnTimeChangedListener;

public class MainScreen extends SherlockActivity {
	/** Called when the activity is first created. */

	boolean enable = true;
	public static final String PREFS_NAME = "com.rappasocial.eyesbreak.EyesBreakPrefs";
	public static final String ACTION_REFRESH_SCHEDULE_ALARM = "com.application.ACTION_REFRESH_SCHEDULE_ALARM";
	TextView tvStatus;
	boolean bMondey, bTUESDAY, bWEDNESDAY, bTHURSDAY, bFRIDAY, bSATURDAY,
			bSUNDAY;
	private ShareActionProvider mShareActionProvider;
	ToggleButton toggle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock);
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(false);

		setContentView(R.layout.main);
		tvStatus = (TextView) findViewById(R.id.tvStatus);
		
		PrefsInit();
		/*
		 * TOGGLE ON/OFF Button
		 */
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		toggle = (ToggleButton) findViewById(R.id.toggleButton1);
		toggle.setChecked(settings.getBoolean("ServiceEnabled", false));
		toggle.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!(toggle.isChecked())) {
					stopService(new Intent(MainScreen.this,
							ServiceEyesBreak.class));
				} else {
					startService(new Intent(MainScreen.this,
							ServiceEyesBreak.class));
				}
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("ServiceEnabled", toggle.isChecked());
				editor.commit();
				refreshStatus();
			}
		});

		toggle.setText(null);
		toggle.setTextOn(null);
		toggle.setTextOff(null);
		refreshStatus();

		AdRequest adRequest = new AdRequest();

		// test mode on DEVICE (this example code must be replaced with your
		// device uniquq ID)
		adRequest.addTestDevice(Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID));

		AdView adView = (AdView) findViewById(R.id.ad);

		// Initiate a request to load an ad in test mode.
		// You can keep this even when you release your app on the market,
		// because
		// only emulators and your test device will get test ads. The user will
		// receive real ads.
		adView.loadAd(adRequest);
		
		
		
	}
	public void PrefsInit(){
		
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		
		long WorkTime = settings.getLong("WorkTime", 0);

		long RestTime = settings.getLong("RestTime", 0);

		long InitTime = settings.getLong("InitTime", 0);

		long ShutTime = settings.getLong("ShutTime", 0);
		
		SharedPreferences.Editor editor = settings.edit();
		
		if (WorkTime == 0) {
			
			editor.putLong("WorkTime", 1000*60*40);
			
		}
		
        if (RestTime == 0) {
			
			editor.putLong("RestTime", 1000*60*5);
			
		}
        
        if (InitTime == 0) {
			
			editor.putLong("InitTime", 1000*60*60*8);
			
		}
        
        if (ShutTime == 0) {
			
			editor.putLong("ShutTime", 1000*60*60*17);
			
		}
		
		
		
		
		editor.commit();
		
	}

	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			refreshStatus();

		}
	};

	public void onResume() {
		super.onResume();

		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_REFRESH_SCHEDULE_ALARM);

		this.registerReceiver(this.receiver, filter);
		refreshStatus();
		
	}

	@Override
	protected void onPause() {
		super.onStop();
		this.unregisterReceiver(this.receiver);

	}

	public void refreshStatus() {

		SharedPreferences mySharedPreferences = getSharedPreferences(
				PREFS_NAME, 0);

		if (!mySharedPreferences.getBoolean("ServiceEnabled", false) || noneDayesChecked()) {
			
			tvStatus.setText("");

		} else {

			long WorkIntervalms = mySharedPreferences
					.getLong("WorkTime", 10000);

			long RestIntervalms = mySharedPreferences
					.getLong("RestTime", 10000);

			long InitTime = mySharedPreferences.getLong("InitTime", 10000);

			long ShutTime = mySharedPreferences.getLong("ShutTime", 10000);

			long curTime_ms = System.currentTimeMillis()
					+ getShiftDaysToStartInMillis();

			Calendar calendar = Calendar.getInstance();

			// if (Calendar.WEDNESDAY == calendar.get(Calendar.DAY_OF_WEEK))
			// calendar.getTimeInMillis()

			calendar.setTimeInMillis(curTime_ms);

			// calendar.set(Calendar.YEAR, 2013); //alarmHour from TextView
			// calendar.set(Calendar.MONTH, 4); //alarmMinute from TextView
			// calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			InitTime = InitTime + calendar.getTimeInMillis();
			ShutTime = ShutTime + calendar.getTimeInMillis();
			// curTime_msTimeOnly = curTime_ms - calendar.getTimeInMillis();
			long timetoRefresh = WorkIntervalms + RestIntervalms;
			long timetoStartalarmBreak = InitTime + WorkIntervalms;
			long timetoStartalarmToWork = InitTime + WorkIntervalms
					+ RestIntervalms;

			long timecurshift;
			if (curTime_ms >= InitTime && curTime_ms <= ShutTime
					&& getShiftDaysToStartInMillis() == 0) {
				timecurshift = (WorkIntervalms + RestIntervalms)
						* (long) Math.floor((double) (curTime_ms - InitTime)
								/ (WorkIntervalms + RestIntervalms));
				timetoStartalarmBreak = timetoStartalarmBreak + timecurshift;
				timetoStartalarmToWork = timetoStartalarmToWork + timecurshift;
				if (curTime_ms - (InitTime + timecurshift) > WorkIntervalms) {
					// getDate(System.currentTimeMillis(), "hh:mm dd.MM.yyyy")
					tvStatus.setText(getString(R.string.NextSignalAt) + ": "
							+ getDate(InitTime + timecurshift + WorkIntervalms
									+ RestIntervalms, "HH:mm dd.MM.yyyy"));

				} else {

					tvStatus.setText(getString(R.string.NextSignalAt) + ": "
							+ getDate(InitTime + timecurshift + WorkIntervalms,
									"HH:mm dd.MM.yyyy"));

				}

			} else {

				tvStatus.setText(getString(R.string.NextSignalAt) + ": "
						+ getDate(InitTime + WorkIntervalms, "HH:mm dd.MM.yyyy"));

			}

		}
	}
	
	public boolean noneDayesChecked(){
		
		  SharedPreferences mySharedPreferences = getSharedPreferences(PREFS_NAME, 0);
		  boolean q = true;
		  if(mySharedPreferences.getBoolean("chbMondey", true)){
			  
			  return !q;
			  
		  }
		  else if (mySharedPreferences.getBoolean("chbTuesday", true)) {
			  
			  return !q;
			  
		  }else if (mySharedPreferences.getBoolean("chbWednesday", true)) {
			  
			  return !q;
			  
		  }else if (mySharedPreferences.getBoolean("chbThursday", true)) {
			  
			  return !q;
			  
		  }else if (mySharedPreferences.getBoolean("chbFriday", true)) {
			  
			  return !q;
			  
		  }else if (mySharedPreferences.getBoolean("chbSaturday", true)) {
			  
			  return !q;
			  
		  }else if (mySharedPreferences.getBoolean("chbSunday", true)) {
			  
			  return !q;
			  
		  }
	        
		  return q;
	}

	public long getShiftDaysToStartInMillis() {

		// Calendar calendar = Calendar.getInstance();
		// calendar.setTimeInMillis(System.currentTimeMillis());
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		bMondey = settings.getBoolean("chbMondey", true);
		bTUESDAY = settings.getBoolean("chbTuesday", true);
		bWEDNESDAY = settings.getBoolean("chbWednesday", true);
		bTHURSDAY = settings.getBoolean("chbThursday", true);
		bFRIDAY = settings.getBoolean("chbFriday", true);
		bSATURDAY = settings.getBoolean("chbSaturday", true);
		bSUNDAY = settings.getBoolean("chbSunday", true);

		long WorkIntervalms = settings.getLong("WorkTime", 10000);

		long RestIntervalms = settings.getLong("RestTime", 10000);

		long InitTime = settings.getLong("InitTime", 10000);

		long ShutTime = settings.getLong("ShutTime", 10000);

		long curTime_ms = System.currentTimeMillis();

		Calendar calendar = Calendar.getInstance();

		// if (Calendar.WEDNESDAY == calendar.get(Calendar.DAY_OF_WEEK))
		// calendar.getTimeInMillis()

		calendar.setTimeInMillis(curTime_ms);

		// calendar.set(Calendar.YEAR, 2013); //alarmHour from TextView
		// calendar.set(Calendar.MONTH, 4); //alarmMinute from TextView
		// calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		InitTime = InitTime + calendar.getTimeInMillis();
		ShutTime = ShutTime + calendar.getTimeInMillis();

		long Time_msNext;
		long timecurshift;
		if (curTime_ms >= InitTime && curTime_ms <= ShutTime) {
			timecurshift = (WorkIntervalms + RestIntervalms)
					* (long) Math.floor((double) (curTime_ms - InitTime)
							/ (WorkIntervalms + RestIntervalms));

			// getDate(InitTime, "hh:mm dd.MM.yyyy")
			if (curTime_ms - (InitTime + timecurshift) > WorkIntervalms) {

				Time_msNext = InitTime + timecurshift + WorkIntervalms
						+ RestIntervalms;

			} else {

				Time_msNext = InitTime + timecurshift + WorkIntervalms;

			}

		} else {

			Time_msNext = curTime_ms;

		}

		int numberOfCurrDay = calendar.get(Calendar.DAY_OF_WEEK);
		boolean daysarrayBool[] = { false, bSUNDAY, bMondey, bTUESDAY,
				bWEDNESDAY, bTHURSDAY, bFRIDAY, bSATURDAY };
		int j = numberOfCurrDay;
		if (Time_msNext > ShutTime) {
			j = numberOfCurrDay + 1;
			if (numberOfCurrDay == 7)
				j = 1;
		}

		for (int i = j; i <= 7; i++) {

			if (daysarrayBool[i]) {

				return (i - j + (Time_msNext > ShutTime ? 1:0)) * 1000 * 60 * 60 * 24;

			}
		}

		for (int i = 1; i <= numberOfCurrDay; i++) {

			if (daysarrayBool[i]) {

				return (7 - j + i + (Time_msNext > ShutTime ? 1:0)) * 1000 * 60 * 60 * 24;

			}

		}

		return 0;

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu)
	// {
	// super.onCreateOptionsMenu(menu);
	// CreateMenu(menu);
	// return true;
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item)
	// {
	// return MenuChoice();
	//
	// }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.settings_menu, menu);
		// Fetch and store ShareActionProvider
		MenuItem item = menu.findItem(R.id.menu_item_share);
		mShareActionProvider = (ShareActionProvider) item.getActionProvider();

		return true;

		// menu.add("Settings")
		// .setIcon(R.drawable.cogs)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		//
		// menu.add("Search")
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM |
		// MenuItem.SHOW_AS_ACTION_WITH_TEXT);
		//
		// menu.add("Share")
		// .setIcon(R.drawable.share)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		//
		// menu.add("Fave")
		// .setIcon(R.drawable.thumbs_up)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		//
		// menu.add("MenuLLL")
		// .setIcon(R.drawable.menu)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		//
		// return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menu_item_share:

			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharetext));
//			Uri uriToImage = Uri.parse("android.resource://com.rappasocial.eyesbreak/drawable/eyesbreak_shareimage.png");
			sendIntent.setType("text/plain");
//			sendIntent.putExtra(Intent.EXTRA_STREAM, uriToImage);
//			sendIntent.setType("image/*");
			setShareIntent(sendIntent);
			return true;

		case R.id.menu_item_settings:

			Intent intent = new Intent(MainScreen.this, SettingsActivity.class);
			startActivity(intent);
			return true;
			
		case R.id.menu_item_rate:

			intent = new Intent(Intent.ACTION_VIEW);
		    //Try Google play
		    intent.setData(Uri.parse("market://details?id=com.rappasocial.eyesbreak"));
		    if (MyStartActivity(intent) == false) {
		        //Market (Google play) app seems not installed, let's try to open a webbrowser
		        intent.setData(Uri.parse("https://play.google.com/store/apps/details?com.rappasocial.eyesbreak"));
		        if (MyStartActivity(intent) == false) {
		            //Well if this also fails, we have run out of options, inform the user.
		            Toast.makeText(this, getString(R.string.gplaynotfound), Toast.LENGTH_SHORT).show();
		        }
		    }
			return true;
		
		case R.id.menu_item_getpro:

			intent = new Intent(Intent.ACTION_VIEW);
		    //Try Google play
		    intent.setData(Uri.parse("market://details?id=com.rappasocial.eyesbreakpro"));
		    if (MyStartActivity(intent) == false) {
		        //Market (Google play) app seems not installed, let's try to open a webbrowser
		        intent.setData(Uri.parse("https://play.google.com/store/apps/details?com.rappasocial.eyesbreakpro"));
		        if (MyStartActivity(intent) == false) {
		            //Well if this also fails, we have run out of options, inform the user.
		            Toast.makeText(this, getString(R.string.gplaynotfound), Toast.LENGTH_SHORT).show();
		        }
		    }
			return true;
			
		case R.id.menu_item_about:

			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(getString(R.string.about));
			alertDialog.setMessage(getString(R.string.abouttext));
			alertDialog.show();
			return true;
			
			

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private boolean MyStartActivity(Intent aIntent) {
	    try
	    {
	        startActivity(aIntent);
	        return true;
	    }
	    catch (ActivityNotFoundException e)
	    {
	        return false;
	    }
	}

	// Call to update the share intent
	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	public static String getDate(long milliSeconds, String dateFormat) {
		// Create a DateFormatter object for displaying date in specified
		// format.
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

}