package com.rappasocial.eyesbreak;


import android.app.Activity;
import android.app.SearchManager.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class DaysSettings extends Activity implements OnClickListener {
	

    CheckBox chbMondey, chbTuesday, chbWednesday, chbThursday, chbFriday, chbSaturday, chbSunday; 
    public static final String PREFS_NAME = "com.rappasocial.eyesbreak.EyesBreakPrefs";
    Button btCancel, btSave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
        
        setContentView(R.layout.days_settings);
        
        chbMondey = (CheckBox) findViewById(R.id.chbMondey);
        chbTuesday = (CheckBox) findViewById(R.id.chbTuesday);
        chbWednesday = (CheckBox) findViewById(R.id.chbWednesday);
        chbThursday = (CheckBox) findViewById(R.id.chbThursday);
        chbFriday = (CheckBox) findViewById(R.id.chbFriday);
        chbSaturday = (CheckBox) findViewById(R.id.chbSaturday);
        chbSunday = (CheckBox) findViewById(R.id.chbSunday);
        
        CheckBoxesInit();
        
        btSave = (Button) findViewById(R.id.btSave);
        btCancel = (Button) findViewById(R.id.btCancel);
        
        btSave.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        
        getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        
        
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btCancel:

			Intent _result = new Intent();
			setResult(Activity.RESULT_OK, _result);
			finish();

			break;
		case R.id.btSave:

			SharedPreferences mySharedPreferences = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = mySharedPreferences.edit();
			
    		editor.putBoolean("chbMondey", chbMondey.isChecked());
    		editor.putBoolean("chbTuesday", chbTuesday.isChecked());
    		editor.putBoolean("chbWednesday", chbWednesday.isChecked());
    		editor.putBoolean("chbThursday", chbThursday.isChecked());
    		editor.putBoolean("chbFriday", chbFriday.isChecked());
    		editor.putBoolean("chbSaturday", chbSaturday.isChecked());
    		editor.putBoolean("chbSunday", chbSunday.isChecked());

    		editor.commit();
    		
    		if (mySharedPreferences.getBoolean("ServiceEnabled", false)){
    			
    			this.stopService(new Intent(this,
						ServiceEyesBreak.class));
    			this.startService(new Intent(this,
						ServiceEyesBreak.class));
    		}
    		_result = new Intent();
			setResult(Activity.RESULT_OK, _result);
    		finish();
			
			break;
		

		}

		
	}
	
	void CheckBoxesInit() {
		
        SharedPreferences mySharedPreferences = getSharedPreferences(PREFS_NAME, 0);
		
        chbMondey.setChecked(mySharedPreferences.getBoolean("chbMondey", true));
        chbTuesday.setChecked(mySharedPreferences.getBoolean("chbTuesday", true));
        chbWednesday.setChecked(mySharedPreferences.getBoolean("chbWednesday", true));
        chbThursday.setChecked(mySharedPreferences.getBoolean("chbThursday", true));
        chbFriday.setChecked(mySharedPreferences.getBoolean("chbFriday", true));
        chbSaturday.setChecked(mySharedPreferences.getBoolean("chbSaturday", true));
        chbSunday.setChecked(mySharedPreferences.getBoolean("chbSunday", true));
		
	}



}
