package com.rappasocial.eyesbreak;

import java.util.Calendar;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.Toast;

public class ScheduleAlarmReceiver extends BroadcastReceiver {

	public static final String ACTION_REFRESH_SCHEDULE_ALARM = "com.application.ACTION_REFRESH_SCHEDULE_ALARM";

	public static final String PREFS_NAME = "com.rappasocial.eyesbreak.EyesBreakPrefs";
	boolean enable = true;
	boolean vibratorEn = true;
	boolean bMondey, bTUESDAY, bWEDNESDAY, bTHURSDAY, bFRIDAY, bSATURDAY,
			bSUNDAY;
	int wait, mInit, mShut = 0;

	static final int TIME_DIALOG_INIT = 0;
	static final int TIME_DIALOG_SHUT = 1;

	Context context;

	private MediaPlayer mMediaPlayer;
	private MediaPlayer mMediaPlayer2;
	private ServiceEyesBreak myServiceBinder;

	@Override
	public void onReceive(Context context, Intent intent) {
		// Intent startIntent = new Intent(context, SmokerReducerService.class);
		// context.startService(startIntent);

		SharedPreferences settings = context
				.getSharedPreferences(PREFS_NAME, 0);
		Uri alert = Uri.parse(settings.getString("SoundURI", ""));
		long ShutTime = settings.getLong("ShutTime", 0);
		long InitTime = settings.getLong("InitTime", 0);
		bMondey = settings.getBoolean("chbMondey", true);
		bTUESDAY = settings.getBoolean("chbTuesday", true);
		bWEDNESDAY = settings.getBoolean("chbWednesday", true);
		bTHURSDAY = settings.getBoolean("chbThursday", true);
		bFRIDAY = settings.getBoolean("chbFriday", true);
		bSATURDAY = settings.getBoolean("chbSaturday", true);
		bSUNDAY = settings.getBoolean("chbSunday", true);

		long curTime_ms = System.currentTimeMillis();

		Calendar calendar = Calendar.getInstance();
		// if (Calendar.WEDNESDAY == calendar.get(Calendar.DAY_OF_WEEK))
		// calendar.getTimeInMillis()

		calendar.setTimeInMillis(System.currentTimeMillis());

		if (isDayOfWeekChecked(calendar.get(Calendar.DAY_OF_WEEK))) {

			// calendar.set(Calendar.YEAR, 2013); //alarmHour from TextView
			// calendar.set(Calendar.MONTH, 4); //alarmMinute from TextView
			// calendar.set(Calendar.DAY_OF_MONTH, 1);

			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);

			InitTime = calendar.getTimeInMillis() + InitTime;
			ShutTime = calendar.getTimeInMillis() + ShutTime;

			if (curTime_ms >= InitTime && curTime_ms <= ShutTime) {

				if (settings.getString("SoundURI", "") != "") {
					try {
						mMediaPlayer = new MediaPlayer();
						mMediaPlayer.setDataSource(context, alert);

						mMediaPlayer.setLooping(false);
						mMediaPlayer.prepare();
						mMediaPlayer.start();

					} catch (Exception e) {

					}
				}
				if (settings.getBoolean("Vibro", false)) {
					Vibrator vibrator = (Vibrator) context
							.getSystemService(Context.VIBRATOR_SERVICE);
					vibrator.vibrate(1000);
				}

				Bundle extras = intent.getExtras();
				if (extras != null) {
					Toast.makeText(context, extras.getString("TostText"),
							Toast.LENGTH_LONG).show();

				}

			} else if (curTime_ms > ShutTime) {

				context.stopService(new Intent(context, ServiceEyesBreak.class));
				context.startService(new Intent(context, ServiceEyesBreak.class));
				// intent = new Intent(this,
				// MyService.class).putExtra(PARAM_TIME, 7)
				// .putExtra(PARAM_TASK, TASK1_CODE);
				// // �������� ������
				// startService(intent);
			}
		}

		// playAudio();

	}

	public ServiceConnection myConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder binder) {
			myServiceBinder = ((ServiceEyesBreak.MyBinder) binder).getService();
			Log.d("ServiceConnection", "connected");
			// showServiceData();
		}

		public void onServiceDisconnected(ComponentName className) {
			Log.d("ServiceConnection", "disconnected");
			myServiceBinder = null;
		}
	};

	public Handler myHandler = new Handler() {
		// public void handleMessage(Message message) {
		// Bundle data = message.getData();
		// }
	};

	public void doBindService() {
		Intent intent = null;
		intent = new Intent(context, ServiceEyesBreak.class);
		// Create a new Messenger for the communication back
		// From the Service to the Activity
		// Messenger messenger = new Messenger(myHandler);
		// intent.putExtra("MESSENGER", messenger);

		context.bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
	}

	boolean isDayOfWeekChecked(int calendarDayOfWeek) {

		if (calendarDayOfWeek == Calendar.MONDAY && bMondey) {

			return true;

		} else if (calendarDayOfWeek == Calendar.TUESDAY && bTUESDAY) {

			return true;

		} else if (calendarDayOfWeek == Calendar.WEDNESDAY && bWEDNESDAY) {

			return true;

		} else if (calendarDayOfWeek == Calendar.THURSDAY && bTHURSDAY) {

			return true;

		} else if (calendarDayOfWeek == Calendar.FRIDAY && bFRIDAY) {

			return true;

		} else if (calendarDayOfWeek == Calendar.SATURDAY && bSATURDAY) {

			return true;

		} else if (calendarDayOfWeek == Calendar.SUNDAY && bSUNDAY) {

			return true;

		} else {

			return false;
		}

	}

	private void playAudio() {
		try {

			// if(enable)
			// {
			// Toast.makeText(context, "playAudio", Toast.LENGTH_SHORT).show();
			mMediaPlayer.seekTo(500);
			// mMediaPlayer.start();
			//
			// mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			// public void onCompletion(MediaPlayer arg0) {
			// mMediaPlayer2.seekTo(300);
			// mMediaPlayer2.start();
			// }
			// });
			//
			// if(vibratorEn)
			// {
			Vibrator vibrator = (Vibrator) this.context
					.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(1000);
			// }

			// }
			// else
			// {
			//
			// }

		} catch (Exception e) {

		}
	}

}
