package com.rappasocial.eyesbreak;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.holoeverywhere.app.AlertDialog;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.support.v4.app.NavUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.TimePicker.OnTimeChangedListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.google.ads.AdRequest;
import com.google.ads.AdView;


public class SettingsActivity extends SherlockActivity {

	TextView tvInit, tvShut, tvWork, tvRest;
	boolean vibratorEn = true;
	int wait, mInit, mShut = 0;
	int WorkIntervalms;
	int RestIntervalms;
	int lasthour = 25;
	static final int TIME_DIALOG_INIT = 0;
	static final int TIME_DIALOG_SHUT = 1;
	ToggleButton tbVibro;
	SeekBar seekbar1;
	AudioManager audioManager;
	public static final String PREFS_NAME = "com.rappasocial.eyesbreak.EyesBreakPrefs";
	RingtoneManager mRingtoneManager;
	TextView spSound, tvDaysSettings;
	String SoundURI;
	Intent Mringtone;
	private MediaPlayer mMediaPlayer;
	private ShareActionProvider mShareActionProvider;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.Theme_Sherlock);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.settings_activity);

		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setTitle(getString(R.string.settings));

		// startService(new Intent(this,ServiceEyesBreak.class));

		// == Preff
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

		seekbar1 = (SeekBar) findViewById(R.id.seekBar1);
		spSound = (TextView) findViewById(R.id.spSound);
		Uri ringtonename = Uri.parse(settings.getString("SoundURI",
				getString(R.string.choosesound)));
		if ((ringtonename.toString()).compareToIgnoreCase(getString(R.string.choosesound)) != 0) {

			Ringtone r = RingtoneManager.getRingtone(this, ringtonename);
			try {
				ringtonename = Uri.parse(r.getTitle(this));
			} catch (Exception e) {
				ringtonename = Uri.parse(getString(R.string.choosesound));
			}

		}

		spSound.setText(ringtonename.toString());
		spSound.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Mringtone = new Intent(mRingtoneManager.ACTION_RINGTONE_PICKER);

				// specifies what type of tone we want, in this case "ringtone",
				// can be notification if you want
				Mringtone.putExtra(mRingtoneManager.EXTRA_RINGTONE_TYPE,
						RingtoneManager.TYPE_NOTIFICATION);

				// gives the title of the RingtoneManager picker title
				Mringtone.putExtra(mRingtoneManager.EXTRA_RINGTONE_TITLE,
						"This is the title Of Your Picker!");

				// returns true shows the rest of the song on the device in the
				// default location
				Mringtone.getBooleanExtra(
						mRingtoneManager.EXTRA_RINGTONE_INCLUDE_DRM, true);

				String uri = null;
				// chooses and keeps the selected item as a uri
				if (uri != null) {
					Mringtone.putExtra(
							mRingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
							Uri.parse(uri));
				} else {
					Mringtone.putExtra(
							mRingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
							(Uri) null);
				}

				startActivityForResult(Mringtone, 0);

			}
		});

		tvDaysSettings = (TextView) findViewById(R.id.tvDaysSettings);
		tvDaysSettings.setText(getDaysPrefsString());
		tvDaysSettings.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(SettingsActivity.this,
						DaysSettings.class);
				startActivityForResult(intent, 5);

			}
		});

		tbVibro = (ToggleButton) findViewById(R.id.tbVibro);
		tbVibro.setChecked(settings.getBoolean("Vibro", false));
		tbVibro.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putBoolean("Vibro", tbVibro.isChecked());
				editor.commit();
			}
		});

		long WorkTime = settings.getLong("WorkTime", 1000 * 60 * 40);

		long RestTime = settings.getLong("RestTime", 1000 * 60 * 5);

		long InitTime = settings.getLong("InitTime", 1000 * 60 * 60 * 8);

		long ShutTime = settings.getLong("ShutTime", 1000 * 60 * 60 * 17);

		tvInit = (TextView) findViewById(R.id.tvInit);

		Calendar calendar = GregorianCalendar.getInstance(TimeZone
				.getTimeZone("GMT+0"));

		calendar.setTimeInMillis(InitTime);

		calendar.get(Calendar.MINUTE);
		tvInit.setText(string2chars(String.valueOf(calendar
				.get(Calendar.HOUR_OF_DAY)))
				+ ":"
				+ string2chars(String.valueOf(calendar.get(Calendar.MINUTE))));

		tvInit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(SettingsActivity.this,
						TimePickerActivity.class);
				intent.putExtra("time", tvInit.getText());
				startActivityForResult(intent, 1);

			}
		});

		audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		int maxVolume = audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		int curVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		seekbar1.setMax(maxVolume);
		seekbar1.setProgress(curVolume);
		seekbar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				playAudio();
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				// TODO Auto-generated method stub
				audioManager
						.setStreamVolume(AudioManager.STREAM_MUSIC, arg1, 0);
			}
		});

		tvShut = (TextView) findViewById(R.id.tvShut);
		calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT+0"));
		calendar.setTimeInMillis(ShutTime);
		tvShut.setText(string2chars(String.valueOf(calendar
				.get(Calendar.HOUR_OF_DAY)))
				+ ":"
				+ string2chars(String.valueOf(calendar.get(Calendar.MINUTE))));
		tvShut.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(SettingsActivity.this,
						TimePickerActivity.class);
				intent.putExtra("time", tvShut.getText());
				startActivityForResult(intent, 2);

			}
		});

		tvWork = (TextView) findViewById(R.id.tvWork);
		calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT+0"));
		calendar.setTimeInMillis(WorkTime);
		tvWork.setText(string2chars(String.valueOf(calendar
				.get(Calendar.HOUR_OF_DAY)))
				+ ":"
				+ string2chars(String.valueOf(calendar.get(Calendar.MINUTE))));
		tvWork.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(SettingsActivity.this,
						TimePickerActivity.class);
				intent.putExtra("time", tvWork.getText());
				startActivityForResult(intent, 3);

			}
		});

		tvRest = (TextView) findViewById(R.id.tvRest);
		calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT+0"));
		calendar.setTimeInMillis(RestTime);
		tvRest.setText(string2chars(String.valueOf(calendar
				.get(Calendar.HOUR_OF_DAY)))
				+ ":"
				+ string2chars(String.valueOf(calendar.get(Calendar.MINUTE))));
		tvRest.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent intent = new Intent(SettingsActivity.this,
						TimePickerActivity.class);
				intent.putExtra("time", tvRest.getText());
				startActivityForResult(intent, 4);

			}
		});

		AdRequest adRequest = new AdRequest();

		// test mode on DEVICE (this example code must be replaced with your
		// device uniquq ID)
		adRequest.addTestDevice(Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID));

		AdView adView = (AdView) findViewById(R.id.ad);

		// Initiate a request to load an ad in test mode.
		// You can keep this even when you release your app on the market,
		// because
		// only emulators and your test device will get test ads. The user will
		// receive real ads.
		adView.loadAd(adRequest);

	}

	public String getDaysPrefsString() {
		String outstr = "";

		SharedPreferences mySharedPreferences = getSharedPreferences(
				PREFS_NAME, 0);

		if (mySharedPreferences.getBoolean("chbMondey", true)) {

			outstr = outstr + getString(R.string.MondeyShort) + ", ";

		}

		if (mySharedPreferences.getBoolean("chbTuesday", true)) {

			outstr = outstr + getString(R.string.TuesdayShort) + ", ";

		}

		if (mySharedPreferences.getBoolean("chbWednesday", true)) {

			outstr = outstr + getString(R.string.WednesdayShort) + ", ";

		}

		if (mySharedPreferences.getBoolean("chbThursday", true)) {

			outstr = outstr + getString(R.string.ThursdayShort) + ", ";

		}

		if (mySharedPreferences.getBoolean("chbFriday", true)) {

			outstr = outstr + getString(R.string.FridayShort) + ", ";

		}

		if (mySharedPreferences.getBoolean("chbSaturday", true)) {

			outstr = outstr + getString(R.string.SaturdayShort) + ", ";

		}

		if (mySharedPreferences.getBoolean("chbSunday", true)) {

			outstr = outstr + getString(R.string.SundayShort) + ", ";

		}

		if (outstr != "") {

			outstr = outstr.substring(0, outstr.length() - 2);

		}

		return outstr;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.settings_menu_min, menu);

		return true;

	}

	private Date stringToDate(String aDate, String aFormat) {

		if (aDate == null)
			return null;
		ParsePosition pos = new ParsePosition(0);
		SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
		Date stringDate = simpledateformat.parse(aDate, pos);
		return stringDate;

	}

	public String string2chars(String instr) {

		if (instr.length() == 1) {

			return "0" + instr;

		}

		return instr;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {

		case android.R.id.home:

			NavUtils.navigateUpTo(this, new Intent(this, MainScreen.class));
			return true;

		case R.id.menu_item_sharemin:

			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sharetext));
//			Uri uriToImage = Uri.parse("android.resource://com.rappasocial.eyesbreak/drawable/eyesbreak_shareimage.png");
			sendIntent.setType("text/plain");
//			sendIntent.putExtra(Intent.EXTRA_STREAM, uriToImage);
//			sendIntent.setType("image/*");
			setShareIntent(sendIntent);
			return true;

		case R.id.menu_item_ratemin:

			Intent intent = new Intent(Intent.ACTION_VIEW);
			// Try Google play
			intent.setData(Uri
					.parse("market://details?id=com.rappasocial.eyesbreak"));
			if (MyStartActivity(intent) == false) {
				// Market (Google play) app seems not installed, let's try to
				// open a webbrowser
				intent.setData(Uri
						.parse("https://play.google.com/store/apps/details?com.rappasocial.eyesbreak"));
				if (MyStartActivity(intent) == false) {
					// Well if this also fails, we have run out of options,
					// inform the user.
					Toast.makeText(this, getString(R.string.gplaynotfound),
							Toast.LENGTH_SHORT).show();
				}
			}
			return true;
			
		case R.id.menu_item_getpro:

			intent = new Intent(Intent.ACTION_VIEW);
		    //Try Google play
		    intent.setData(Uri.parse("market://details?id=com.rappasocial.eyesbreakpro"));
		    if (MyStartActivity(intent) == false) {
		        //Market (Google play) app seems not installed, let's try to open a webbrowser
		        intent.setData(Uri.parse("https://play.google.com/store/apps/details?com.rappasocial.eyesbreakpro"));
		        if (MyStartActivity(intent) == false) {
		            //Well if this also fails, we have run out of options, inform the user.
		            Toast.makeText(this, getString(R.string.gplaynotfound), Toast.LENGTH_SHORT).show();
		        }
		    }
			return true;

		case R.id.menu_item_aboutmin:

			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(getString(R.string.about));
			alertDialog.setMessage(getString(R.string.abouttext));
			alertDialog.show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void setShareIntent(Intent shareIntent) {
		if (mShareActionProvider != null) {
			mShareActionProvider.setShareIntent(shareIntent);
		}
	}

	private boolean MyStartActivity(Intent aIntent) {
		try {
			startActivity(aIntent);
			return true;
		} catch (ActivityNotFoundException e) {
			return false;
		}
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent Mringtone) {

		if (requestCode == 0) {
			switch (resultCode) {
			/*
    	* 
    	*/
			case RESULT_OK:
				// sents the ringtone that is picked in the Ringtone Picker
				// Dialog
				Uri uri = Mringtone
						.getParcelableExtra(mRingtoneManager.EXTRA_RINGTONE_PICKED_URI);

				// send the output of the selected to a string
				if (uri != null) {
					SoundURI = uri.toString();
					Ringtone r = RingtoneManager.getRingtone(this, uri);
					Uri ringtonename;
					try {
						ringtonename = Uri.parse(r.getTitle(this));
					} catch (Exception e) {
						ringtonename = Uri.parse(getString(R.string.choosesound));
					}
					this.spSound.setText(ringtonename.toString());

					try {
						RingtoneManager.setActualDefaultRingtoneUri(this,
								resultCode, uri);
					} catch (Exception localException) {

					}
				} else {
					SoundURI = getString(R.string.choosesound);
				}

				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putString("SoundURI", SoundURI);
				editor.commit();

				
				break;

			}
		} else if (requestCode == 1) {

			switch (resultCode) {
			/*
    	* 
    	*/
			case RESULT_OK:

				tvInit.setText(Mringtone.getStringExtra("time"));

				Date ShutDate = stringToDate(tvInit.getText().toString(),
						"HH:mm");
				Calendar cal = new GregorianCalendar();
				cal.setTime(ShutDate);
				int hours = cal.get(Calendar.HOUR_OF_DAY) == 23 ? 0 : cal
						.get(Calendar.HOUR_OF_DAY) + 1;
				int minutes = cal.get(Calendar.MINUTE);
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putLong("InitTime", hours * 60 * 60 * 1000 + minutes
						* 60 * 1000);
				editor.commit();
				if (settings.getBoolean("ServiceEnabled", false)) {

					this.stopService(new Intent(this, ServiceEyesBreak.class));
					this.startService(new Intent(this, ServiceEyesBreak.class));
				}

				break;

			}

		} else if (requestCode == 2) {

			switch (resultCode) {
			/*
    	* 
    	*/
			case RESULT_OK:

				tvShut.setText(Mringtone.getStringExtra("time"));

				Date ShutDate = stringToDate(tvShut.getText().toString(),
						"HH:mm");
				Calendar cal = new GregorianCalendar();
				cal.setTime(ShutDate);
				int hours = cal.get(Calendar.HOUR_OF_DAY) == 23 ? 0 : cal
						.get(Calendar.HOUR_OF_DAY) + 1;
				int minutes = cal.get(Calendar.MINUTE);
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putLong("ShutTime", hours * 60 * 60 * 1000 + minutes
						* 60 * 1000);
				editor.commit();
				if (settings.getBoolean("ServiceEnabled", false)) {

					this.stopService(new Intent(this, ServiceEyesBreak.class));
					this.startService(new Intent(this, ServiceEyesBreak.class));
				}

				break;

			}

		} else if (requestCode == 3) {

			switch (resultCode) {
			/*
    	* 
    	*/
			case RESULT_OK:

				tvWork.setText(Mringtone.getStringExtra("time"));

				Date ShutDate = stringToDate(tvWork.getText().toString(),
						"HH:mm");
				Calendar cal = new GregorianCalendar();
				cal.setTime(ShutDate);
				int hours = cal.get(Calendar.HOUR_OF_DAY) == 23 ? 0 : cal
						.get(Calendar.HOUR_OF_DAY) + 1;
				int minutes = cal.get(Calendar.MINUTE);
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putLong("WorkTime", hours * 60 * 60 * 1000 + minutes
						* 60 * 1000);
				editor.commit();
				if (settings.getBoolean("ServiceEnabled", false)) {

					this.stopService(new Intent(this, ServiceEyesBreak.class));
					this.startService(new Intent(this, ServiceEyesBreak.class));
				}
				// getDate(hours * 60 * 60 * 1000 + minutes * 60 * 1000,
				// "dd/MM/yyyy hh:mm:ss.SSS")
				break;

			}

		} else if (requestCode == 4) {

			switch (resultCode) {
			/*
    	* 
    	*/
			case RESULT_OK:

				tvRest.setText(Mringtone.getStringExtra("time"));

				Date ShutDate = stringToDate(tvRest.getText().toString(),
						"HH:mm");
				Calendar cal = new GregorianCalendar();
				cal.setTime(ShutDate);
				int hours = cal.get(Calendar.HOUR_OF_DAY) == 23 ? 0 : cal
						.get(Calendar.HOUR_OF_DAY) + 1;
				int minutes = cal.get(Calendar.MINUTE);
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				editor.putLong("RestTime", hours * 60 * 60 * 1000 + minutes
						* 60 * 1000);
				editor.commit();
				if (settings.getBoolean("ServiceEnabled", false)) {

					this.stopService(new Intent(this, ServiceEyesBreak.class));
					this.startService(new Intent(this, ServiceEyesBreak.class));
				}

				break;

			}

		} else if (requestCode == 5) {

			switch (resultCode) {
			/*
    	* 
    	*/
			case RESULT_OK:

				tvDaysSettings.setText(getDaysPrefsString());

				break;

			}

		}

	}

	public static String getDate(long milliSeconds, String dateFormat) {
		// Create a DateFormatter object for displaying date in specified
		// format.
		DateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in
		// milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	public void playAudio() {

		SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, 0);
		Uri alert = Uri.parse(settings.getString("SoundURI", ""));

		if (settings.getString("SoundURI", "") == "") {

			Toast.makeText(this, getString(R.string.soundnotchosen),
					Toast.LENGTH_LONG).show();

		} else {

			try {
				mMediaPlayer = new MediaPlayer();
				mMediaPlayer.setDataSource(this, alert);

				mMediaPlayer.setLooping(false);
				mMediaPlayer.prepare();
				mMediaPlayer.start();

			} catch (Exception e) {

			}
		}

		if (settings.getBoolean("Vibro", false)) {
			Vibrator vibrator = (Vibrator) this
					.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(1000);
		}

	}

	@Override
	protected void onPause() {
		super.onStop();

	}

}
