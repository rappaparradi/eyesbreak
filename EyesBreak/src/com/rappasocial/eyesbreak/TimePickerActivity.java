package com.rappasocial.eyesbreak;

import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.NumericWheelAdapter;

import org.holoeverywhere.app.Activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewDebug.IntToString;
import android.widget.Button;
import android.widget.Toast;

public class TimePickerActivity extends Activity implements OnClickListener {

	Button btOk, btCancel;
	WheelView hours;
	WheelView mins;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.time_picker_dialog);

		hours = (WheelView) findViewById(R.id.hour);
		hours.setCyclic(true);
		hours.setViewAdapter(new NumericWheelAdapter(this, 0, 23));

		mins = (WheelView) findViewById(R.id.mins);
		mins.setViewAdapter(new NumericWheelAdapter(this, 0, 59, "%02d"));
		mins.setCyclic(true);

		btOk = (Button) findViewById(R.id.btOk);
		btOk.setOnClickListener(this);
		
		btCancel = (Button) findViewById(R.id.btCancel);
		btCancel.setOnClickListener(this);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String timestr = extras.getString("time");
			
			hours.setCurrentItem(Integer.valueOf(timestr.substring(0, 2)));
			mins.setCurrentItem(Integer.valueOf(timestr.substring(3, 5)));
	
		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btOk:

			Intent _result = new Intent();
			_result.putExtra(
					"time",
					""
							+ string2chars(String.valueOf(hours
									.getCurrentItem()))
							+ ":"
							+ string2chars(String.valueOf(mins.getCurrentItem())));
			setResult(Activity.RESULT_OK, _result);
			finish();

			break;

		case R.id.btCancel:


			finish();

			break;

		}

	}

	public String string2chars(String instr) {

		if (instr.length() == 1) {

			return "0" + instr;

		}

		return instr;

	}

}
